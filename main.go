package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/cretz/bine/tor"
)

const EXPERIMENT_REPEATS = 3
const RECVBUFFER = "SNOWFLAKE_RECVBUFFER"
const STRMBUFFER = "SNOWFLAKE_STRMBUFFER"
const FILE8MB = "https://dist.torproject.org/tor-0.4.6.6.tar.gz"
const FILE20MB = "https://sindominio.net/kali/20MB.zip"
const FILE50MB = "http://ipv4.download.thinkbroadband.com/20MB.zip"

const FILE8MB_SIZE = 7754194
const FILE20MB_SIZE = 20971520

type DataPoint struct {
	batch, rbuf, sbuf, btime, drate interface{}
}

const torrc = `UseBridges 1
DataDirectory datadir

ClientTransportPlugin snowflake exec ./snowflake-client \
-log snowflake-log \
-url https://snowflake-broker.torproject.net.global.prod.fastly.net/ -front cdn.sstatic.net \
-ice stun:stun.voip.blackberry.com:3478,stun:stun.altar.com.pl:3478,stun:stun.antisip.com:3478,stun:stun.bluesip.net:3478,stun:stun.dus.net:3478,stun:stun.epygi.com:3478,stun:stun.sonetel.com:3478,stun:stun.sonetel.net:3478,stun:stun.stunprotocol.org:3478,stun:stun.uls.co.za:3478,stun:stun.voipgate.com:3478,stun:stun.voys.nl:3478 \
-max 3

Bridge snowflake 0.0.3.0:1`

func main() {
	bufvalues := [][]int{
		{4 * 1024 * 1024, 1 * 64 * 1024},
		{4 * 1024 * 1024, 4 * 64 * 1024},
		{4 * 1024 * 1024, 8 * 64 * 1024},
		{4 * 1024 * 1024, 12 * 64 * 1024},
		{4 * 1024 * 1024, 16 * 64 * 1024},
		{4 * 1024 * 1024, 20 * 64 * 1024},
		{4 * 1024 * 1024, 24 * 64 * 1024},
		{4 * 1024 * 1024, 28 * 64 * 1024},
		{4 * 1024 * 1024, 32 * 64 * 1024},
		{6 * 1024 * 1024, 1 * 64 * 1024},
		{6 * 1024 * 1024, 4 * 64 * 1024},
		{6 * 1024 * 1024, 8 * 64 * 1024},
		{6 * 1024 * 1024, 12 * 64 * 1024},
		{6 * 1024 * 1024, 16 * 64 * 1024},
		{6 * 1024 * 1024, 20 * 64 * 1024},
		{6 * 1024 * 1024, 24 * 64 * 1024},
		{6 * 1024 * 1024, 28 * 64 * 1024},
		{6 * 1024 * 1024, 32 * 64 * 1024},
	}

	for index, row := range bufvalues {
		rbuf := row[0]
		sbuf := row[1]
		if err := runExperiment(index, rbuf, sbuf); err != nil {
			log.Fatal(err)
		}
	}

}

func runExperiment(index, receiveBuffer, streamBuffer int) error {

	os.Setenv(RECVBUFFER, strconv.Itoa(receiveBuffer))
	os.Setenv(STRMBUFFER, strconv.Itoa(streamBuffer))

	rcfile := writeTorrc()
	conf := &tor.StartConf{DebugWriter: os.Stdout, TorrcFile: rcfile}

	fmt.Println("Starting Tor...")
	fmt.Printf("STRMBUFFER: %v\n", streamBuffer)
	fmt.Printf("RECVBUFFER: %v\n", receiveBuffer)
	fmt.Println("")

	bt1 := time.Now()
	t, err := tor.Start(nil, conf)
	if err != nil {
		return err
	}

	defer t.Close()

	// Wait at most 20 minutes
	dialCtx, dialCancel := context.WithTimeout(context.Background(), time.Minute*20)
	defer dialCancel()
	dialer, err := t.Dialer(dialCtx, nil)
	if err != nil {
		return err
	}

	client := &http.Client{
		Transport: &http.Transport{
			DialContext: dialer.DialContext,
		},
	}

	fmt.Println("")
	bt2 := time.Now()
	bootstrapTimeSeconds := bt2.Sub(bt1) / 1e9
	fmt.Println("")
	fmt.Printf("Bootstrap time: %d seconds\n", bootstrapTimeSeconds)

	for i := 0; i < EXPERIMENT_REPEATS; i++ {
		rate, err := measureDownload(client)
		if err != nil {
			log.Printf(err.Error())
		}
		logData(index, receiveBuffer, streamBuffer, int(bootstrapTimeSeconds), rate)
	}

	return nil
}

func writeTorrc() string {
	f, err := ioutil.TempFile("", "torrc-snowflake-")
	if err != nil {
		log.Println(err)
	}
	f.Write([]byte(torrc))
	return f.Name()
}

func logData(i, rbuf, sbuf, bootstrap, rate int) {
	file, err := os.OpenFile("data.csv", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		log.Println(err)
	}
	defer file.Close()

	s := fmt.Sprintf("%d,%d,%d,%d,%d\n", i, rbuf, sbuf, bootstrap, rate)
	if _, err := file.WriteString(s); err != nil {
		log.Fatal(err)
	}
}

func measureDownload(client *http.Client) (int, error) {

	req, err := http.NewRequest("GET", FILE20MB, nil)
	if err != nil {
		return -1, err
	}
	req.Header.Set("User-Agent", "Mozilla/5.0")

	// do request ---------
	dt1 := time.Now()
	resp, err := client.Do(req)
	if err != nil {
		return -1, err
	}
	defer resp.Body.Close()
	_, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return -1, err
	}
	dt2 := time.Now()
	// request done -------

	downloadTimeSeconds := dt2.Sub(dt1) / 1e9

	drate := FILE20MB_SIZE / 1024 / downloadTimeSeconds
	fmt.Printf("Download rate: %d KB/s\n", drate)
	defer resp.Body.Close()

	return int(drate), nil
}
