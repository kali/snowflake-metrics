# snowflake-metrics

First, checkout the branch that allows to change buffer size on the client:

```
cd ../ && git clone https://0xacab.org/kali/snowflake
git checkout measure-buffers
cd client && go build
```

Now let's measure some runs:

```
export SNOWFLAKE_STRMBUFFER=$((1 * 1024 * 1024))
export SNOWFLAKE_RECVBUFFER=$((4 * 1024 * 1024))
ln -s ~/src/snowflake/client/client snowflake-client
go build && ./snowflake-metrics
```
You can tail the snowflake log in another window:

```
tail -f snowflake-log
```
